

# INF 19.02

ru.kpfu.itis.group905.<фамилия>.inf.exceptions 

Exceptions

- 1 - 1/1
- 2 - 1/1
- 3 - 1/1
- 4 - 1/1
- 5 - 1/1
- 6 - 1/1

# ALG 21.02 

ru.kpfu.itis.group905.<фамилия>.algorithms.pair1

LinkedList + Sorting + Accountable

- LinkedList - 0/9
- merge - 0/2
- diff - 0/2
- intersection - 0/2
- generic Accountable - 0/5

# INF 21.02 

ru.kpfu.itis.group905.<фамилия>.inf.exceptions

Exceptions 2

- IDNumber - 0/5
- Point - 0/5
- Convert - 0/5
- SumException - 0/5
- RunnerProcessor - 0/5
- ThrowsExceptions - 0/5

# ALG 28.02 

ru.kpfu.itis.group905.<фамилия>.algorithms.pair2

Sorting

- merge - 0/2
- largest number - 0/2

# INF 29.02

ru.kpfu.itis.group905.<фамилия>.inf.generics

Generics

- LinkedList - 0/5
- Array - 0/5
- test on Account<String, Integer> - 0/5

# INF 07.03 

ru.kpfu.itis.group905.<фамилия>.inf.comparable

Comparable

- 1 - 5/5
- 2 - 5/5
- 3 - 0/5

А спросить?)
- 4 - 5/5


# INF 10.03 

ru.kpfu.itis.group905.<фамилия>.inf.maps

Maps

- 1 - 5/5
- 2 - 5/5

# ALG 13.03 

ru.kpfu.itis.group905.<фамилия>.algorithms.pair3

Sets

- 1 - 2/2
- 2 - 2/2

# INF KR1 11.03 10:10

ru.kpfu.itis.group905.<фамилия>.inf.kr1

7/10


# INF 24.03

ru.kpfu.itis.group905.<фамилия>.inf.iterator

Iterator

- 1 - 0/5
- 2 - 0/5
- 3 - 0/5

# ALG 27.03

ru.kpfu.itis.group905.<фамилия>.algorithms.pair4

- 1 - 0/2
- 2 - 0/2
- 3 - 0/2

# INF 30.03

ru.kpfu.itis.group905.<фамилия>.inf.tree

Trees

- 1 - 4/5

justification??
- 2 - 5/5
- 3 - 5/5
- 4 - 5/5

# ALG 03.04

ru.kpfu.itis.group905.<фамилия>.algorithms.pair5

Dynamics

- 1 - 0/2
- 2 - 0/2
- 3 - 0/2


# ALG SEM1 03.04



# INF Stack

ru.kpfu.itis.group905.<фамилия>.inf.stack

* это даты пар, на которых должно было выполняться задание
- Stack (14.04) - 8/10
- RPN (15.04) - 8/10
- RPN reformat (21.04) - 10/10
- combine all (22.04) - 10/10
- sorting (22.04) - 10/10


# ALG 24.04

ru.kpfu.itis.group905.<фамилия>.algorithms.pair7

- 1 - 2/2
- 2 - 2/2




# ALG 01.05

ru.kpfu.itis.group905.<фамилия>.algorithms.pair8

- 1 - 2
- 2 - 2
- 3 - 2
- 4 - 2


# INF 06.05

ru.kpfu.itis.group905.<фамилия>.inf.stack

- reformat - 3


# INF 08.05

ru.kpfu.itis.group905.<фамилия>.inf.strings

- 1 - 5
- 2 - 5
- 3 - 5
- 4 - 5


# INF 11.05

ru.kpfu.itis.group905.<фамилия>.inf.regex

- 1 - 0
- 2 - 5
- 3 - 5


# ALG 15.05

ru.kpfu.itis.group905.<фамилия>.algorithms.pair9

- 1 - 0
- 2 - 0
- 3 - 0
- 4 - 0


# INF 18.05

ru.kpfu.itis.group905.<фамилия>.inf.files

- java files - 5
- url - 5


# ALG 20.05

ru.kpfu.itis.group905.<фамилия>.algorithms.pair10

- 1 - 0
- 2 - 0
- 3 - 0
- 4 - 0


# INF 24.05

ru.kpfu.itis.group905.<фамилия>.inf.streamApi

- 1 - 5
- 2 - 5





# INF 26.05 12:00



- Fuel - [-] 
- Oxidizer - [-]
- Kerosene и Hydrogen - [-]
- LiquidOxygen - [-]
- InsufficientMatterException - [x]
- Tank<T> - [-]
- UnstableEngineException - [x]
- RocketComparator - [-]
- Code style - [x]
- It works - [-]

Total: 3

