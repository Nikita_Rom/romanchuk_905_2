package com.c3.fuels;

import com.c3.exceptions.InsufficientMatterException;

public class Kerosene implements Fuel {
    public Kerosene(int i) {
    }

    @Override
    public int getCarbonAtomsCount() {
        return 12;
    }

    @Override
    public int getHydrogenAtomsCount() {
        return 26;
    }

    @Override
    public int getMass() {
        return 0;
    }

    @Override
    public void decreaseMass(int mass) throws InsufficientMatterException {

    }
}
