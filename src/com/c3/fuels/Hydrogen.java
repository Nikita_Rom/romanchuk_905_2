package com.c3.fuels;

import com.c3.exceptions.InsufficientMatterException;

public class Hydrogen implements Fuel {
    @Override
    public int getMass() {
        return 0;
    }

    @Override
    public void decreaseMass(int mass) throws InsufficientMatterException {

    }

    @Override
    public int getHydrogenAtomsCount() {
        return 2;
    }
}
