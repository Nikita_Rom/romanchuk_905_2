package com.c3.fuels;

import com.c3.common.IMatter;

public interface Fuel extends IMatter {

    default int getCarbonAtomsCount() { return 6; }
    default int getHydrogenAtomsCount() {
        return 12;
    }
    default int getOxygenAtomsCount() {
        return 6;
    }
    default int getNitrogenAtomsCount() {
        return 0;
    }
    // C6H12O6
}
