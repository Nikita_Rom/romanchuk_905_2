package com.c3.oxidizers;

import com.c3.common.IMatter;

public interface Oxidizer extends IMatter {
    default int getOxygenAtomsCount() {
        return 4;
    }
    default int getNitrogenAtomsCount() {
        return 2;
    }
}
