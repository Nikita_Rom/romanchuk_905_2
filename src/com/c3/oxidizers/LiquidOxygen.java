package com.c3.oxidizers;

import com.c3.exceptions.InsufficientMatterException;

public class LiquidOxygen implements Oxidizer {

    @Override
    public int getMass() {
        return 0;
    }

    @Override
    public void decreaseMass(int mass) throws InsufficientMatterException {

    }

    @Override
    public int getOxygenAtomsCount() {
        return 2;
    }
}
