package com.c3.exceptions;

public class InsufficientMatterException extends Exception {
    int left;

    public InsufficientMatterException(int mass) {
        left = mass;
    }

    public String toString() {
        return ("Custom Exception Occurred: " + left);
    }

    public Object getLeft() {
        return left;
    }
}
