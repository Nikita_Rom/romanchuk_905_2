package com.c3.common;

import com.c3.exceptions.InsufficientMatterException;
import com.c3.exceptions.UnstableEngineException;
import com.c3.fuels.Fuel;
import com.c3.oxidizers.Oxidizer;

import java.util.Random;

/**
 * Ракета
 */
public class Rocket {
    private Tank<? extends Fuel> fuelTank;
    private Tank<? extends Oxidizer> oxidizerTank;
    private int fuelPart;
    private int oxidizerPart;
    private Random r;

    public Rocket(){
        fuelTank = null;
        oxidizerTank = null;
        fuelPart = 1;
        oxidizerPart = 1;
        r = new Random();
    }
    public void connectFuelTank(Tank<? extends Fuel> fuelTank){
        this.fuelTank = fuelTank;
    }
    public void connectOxidizerTank(Tank<? extends Oxidizer> oxidizerTank){
        this.oxidizerTank = oxidizerTank;
    }
    public void setFuelPart(int fuelPart) {
        this.fuelPart = fuelPart;
    }

    public void setOxidizerPart(int oxidizerPart) {
        this.oxidizerPart = oxidizerPart;
    }

    public int burn() throws UnstableEngineException {
        Fuel fuel = null;
        try {
            fuel = fuelTank.next(fuelPart);
            Oxidizer oxidizer = null;
            try {
                oxidizer = oxidizerTank.next(oxidizerPart);
                int carbon = fuelPart*fuel.getCarbonAtomsCount();
                int hydrogen = fuelPart*fuel.getHydrogenAtomsCount();
                int nitrogen = fuelPart*fuel.getNitrogenAtomsCount() + oxidizerPart*oxidizer.getNitrogenAtomsCount();
                int oxygen = fuelPart*fuel.getOxygenAtomsCount() + oxidizerPart*oxidizer.getOxygenAtomsCount();

                int notBurned = Math.abs(2*carbon + hydrogen - 5*oxygen);

                if (r.nextInt(100) == 1)
                    throw new UnstableEngineException("Unknown reason");

                return 2*carbon + hydrogen - notBurned + r.nextInt(3);
            } catch (InsufficientMatterException e) {
                throw new UnstableEngineException(0, e.getLeft());
            }
        } catch (InsufficientMatterException e) {
            throw new UnstableEngineException(e.getLeft(), 0);
        }
    }
}
