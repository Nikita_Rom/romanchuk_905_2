package ru.kpfu.itis.group905.Romanchuk.inf.maps;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Task_2 {
    public static void main(String[] args) throws IOException {
        String file = "input.txt";
        final String path = "C:\\Users\\User\\Desktop\\JAVA Albina\\romanchuk_905_2\\src\\ru\\kpfu\\itis\\group905\\Romanchuk\\inf\\maps\\";
        BufferedReader inFile = new BufferedReader(new FileReader(path + file));

        HashMap<String, Integer> HashMap = new HashMap<>();

        String string = inFile.readLine();
        int k = 1;
        for (String temp : string.split(" ")) {
            if (HashMap.containsKey(temp.toLowerCase())) {
                HashMap.put(temp, HashMap.get(temp) + 1);
            }
            else {
                HashMap.put(temp, 1);
            }
        }
        println(HashMap);
    }

    public static void println(HashMap HMap) {
        HashMap<String, Integer> HashMap = HMap;
        // this.HashMap = HashMap;
        for(Map.Entry entry : HashMap.entrySet()) {
            if ((int) entry.getValue() == 2 || (int) entry.getValue() == 3 || (int) entry.getValue() == 4) {
                System.out.println(entry.getKey() + " : " + entry.getValue() + " раза");
            }
            else {
                System.out.println(entry.getKey() + " : " + entry.getValue() + " раз");
            }
        }
    }
}
