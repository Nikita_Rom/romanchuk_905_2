package ru.kpfu.itis.group905.Romanchuk.inf.maps;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Task_1 {
    public static void main(String[] args) throws IOException {
        String file = "input.txt";
        final String path = "C:\\Users\\User\\Desktop\\JAVA Albina\\romanchuk_905_2\\src\\ru\\kpfu\\itis\\group905\\Romanchuk\\inf\\maps\\";
        BufferedReader inFile = new BufferedReader(new FileReader(path + file));

        HashMap<Integer, Integer> HashMap = new HashMap<>();

        String line;
        while ((line = inFile.readLine()) != null) {
            int temp = Integer.parseInt(line);
            if (HashMap.containsKey(temp)) {
                HashMap.put(temp, HashMap.get(temp) + 1);
            }
            else {
                HashMap.put(temp, 1);
            }

        }
        inFile.close();

        Task_2.println(HashMap);
    }
}
