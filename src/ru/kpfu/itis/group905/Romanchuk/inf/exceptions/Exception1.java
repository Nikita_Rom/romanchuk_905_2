package ru.kpfu.itis.group905.Romanchuk.inf.exceptions;

import java.io.*;

/*
    Полагаю, что это не с компилится потому как
    в классе не написано, что он является ребенком
    ошибок, Trowable или, в частности, Exception.

    UPD: работает так, как я и предполагал
 */

public class Exception1 extends Exception {
    public static void main(String[] args) {
        try {
            throw new Exception1();
        } catch (Exception1 e) {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }
    }
}
