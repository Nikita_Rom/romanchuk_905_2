package ru.kpfu.itis.group905.Romanchuk.inf.exceptions;

import java.io.*;

/*
    Должен вывести все буквы, кроме с, т.к. туда мы не попадаем
 */
public class Exception4 {
    public static void main(String[] args) {
        System.out.println("a");
        try {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }

        System.out.println("e");
    }
}
