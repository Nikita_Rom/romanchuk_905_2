package ru.kpfu.itis.group905.Romanchuk.inf.exceptions;

import java.io.*;

/*
    Должен вывести 10, зайти в файнали
 */
public class Exception5 {
    public static int divide(int a, int b) {
        try {
            return a/b;
        } catch (Exception e) {
            return 0;
        } finally {
            return 10;
        }
    }

    public static void main(String[] args) {
        int i = divide(11, 1);
        System.out.println(i);
    }
}
