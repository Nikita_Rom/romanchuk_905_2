package ru.kpfu.itis.group905.Romanchuk.inf.exceptions;

/*

 */
public class Exception6 extends Exception {
    public static void main(String[] args) {
        System.out.println("Hi!");
        try {
            System.out.println("a");
            throw new Exception6();
        } catch (Exception6 e) {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }

        System.out.println("e");
    }
}
