package ru.kpfu.itis.group905.Romanchuk.inf.exceptions;

import java.io.*;

/*
    Должно скомпилиться и вывести a и b и вылететь на кетче, 16 строка
    UPD: все в порядке
 */
public class Exception3 {
    public static void main(String[] args) throws Exception {
        try {
            System.out.println("a");
            throw new NullPointerException();
        } catch (NullPointerException e) {
            System.out.println("b");
            throw new Exception();
        } catch (Exception e) {
            System.out.println("c");
        }
    }
}
