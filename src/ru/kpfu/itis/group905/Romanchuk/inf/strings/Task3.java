package ru.kpfu.itis.group905.Romanchuk.inf.strings;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) throws FileNotFoundException {
        String str;
        Task3 Task = new Task3();

        try (Scanner scanner = new Scanner(new File("src\\ru\\kpfu\\itis\\group905" +
                "\\Romanchuk\\inf\\strings\\in.txt"))) {
            str = scanner.next();
            Task.replace(str);
            while (scanner.hasNext()) {
                str = scanner.next();
                Task.replace(str);
            }
        }
    }

    public void replace(String string) {
        StringBuilder strBuild = new StringBuilder(string);
        for (int i = 0; i < string.length() - 2; i++) {
            if ((strBuild.charAt(i) == 'm') && (strBuild.charAt(i + 1) == 'o')
                    && (strBuild.charAt(i + 2) == 'm')) {
                strBuild.setCharAt(i, 'd');
                strBuild.setCharAt(i + 1, 'a');
                strBuild.setCharAt(i + 2, 'd');
            }
        }
        System.out.println(strBuild.toString());
    }
}
