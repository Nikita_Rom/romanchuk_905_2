package ru.kpfu.itis.group905.Romanchuk.inf.strings;

public class MyStrings implements Comparable<String> {

    public void MyStrings(String string) {
        String[] words = string.split(" ");

        boolean isSorted = false;
        String buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < words.length - 1; i++) {
                if (words[i].compareTo(words[i + 1]) > 0) {
                    isSorted = false;
                    buf = words[i];
                    words[i] = words[i + 1];
                    words[i + 1] = buf;
                }
            }
        }

        StringBuilder strBuild = new StringBuilder();
        for (String word : words) {
            strBuild.append(word).append(" ");
        }
        System.out.println("sort: " + strBuild.toString());
    }

    @Override
    public int compareTo(String o) {
        return 0;
    }
}
