package ru.kpfu.itis.group905.Romanchuk.inf.strings;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) throws FileNotFoundException {
        String str;
        Task4 Task = new Task4();

        try (Scanner scanner = new Scanner(new File("src\\ru\\kpfu\\itis\\group905" +
                "\\Romanchuk\\inf\\strings\\in.txt"))) {
            str = scanner.nextLine();
        }

        str = Task.joinOdd(str);
        System.out.println(str);
    }

    public String joinOdd(String string) {
        String[] words = string.split(" ");
        StringBuilder strBuild = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            strBuild.append(words[i]).append(" ");
            i++;
        }
        return strBuild.toString();
    }
}
