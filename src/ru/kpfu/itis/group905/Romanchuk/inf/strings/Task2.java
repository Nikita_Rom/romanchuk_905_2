package ru.kpfu.itis.group905.Romanchuk.inf.strings;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) throws FileNotFoundException {
        String str;
        try (Scanner scanner = new Scanner(new File("src\\ru\\kpfu\\itis\\group905" +
                "\\Romanchuk\\inf\\strings\\in.txt"))) {
            str = scanner.next();
            StringBuilder strBuilder = new StringBuilder(str);
            while (scanner.hasNext()) {
                strBuilder.append(scanner.next().toLowerCase());
            }
            count(strBuilder);
        }
    }

    public static void count(StringBuilder str) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            if (!map.containsKey(str.charAt(i))) {
                map.put(str.charAt(i), 1);
            } else {
                map.put(str.charAt(i), map.get(str.charAt(i)) + 1);
            }
        }
        System.out.println(map);
    }
}
