package ru.kpfu.itis.group905.Romanchuk.inf.strings;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) throws FileNotFoundException {
        String str;
        MyStrings MyStrings = new MyStrings();

        try (Scanner scanner = new Scanner(new File("src\\ru\\kpfu\\itis\\group905" +
                "\\Romanchuk\\inf\\strings\\in.txt"))) {
            str = scanner.nextLine();
        }
        System.out.println(str);
        MyStrings.MyStrings(str);
    }
}
