package ru.kpfu.itis.group905.Romanchuk.inf;

/*
Write the implementation of methods marked with TODO and check them in main
Don't forget to sign out your code! (this sourse file)
*/

public class Array {

    private int[] a;
    private int index;

    public Array(int[] ints) {
        a = new int[100];
        index = 0;
    }

    // adding x to the end of array
    public void add(int x) {
        if (index < a.length) {
            a[index] = x;
            index += 1;
        }
    }

    // adding x on position k. Be sure that position k is applicable to actual size of array.
    public void add(int k, int x) {
        if ((k < a.length) || (k >= 0)) {
            a[k] = x;
        }
    }

    // clearing the array
    public void clear() {
        index = 0;
    }

    // check if x is in array
    public boolean contains(int x) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] == x) return true;
        }
        return false;
    }

    // remove an element from position k
    public void remove(int k) {
        if ((k < a.length) || (k >= 0)) {
            a[k] = 0;
        }
    }

    // remove first occurence of x in array
    public void delete(int x) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] == x) {
                a[i] = 0;
                return;
            }
        }
    }

    // convert array to string
    public String toString() {
        System.out.println("[");
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i] + " ");
        }
        System.out.println("]");
        return " ";
    }

}
