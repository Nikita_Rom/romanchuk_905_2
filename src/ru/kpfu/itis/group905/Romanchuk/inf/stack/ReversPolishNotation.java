package ru.kpfu.itis.group905.Romanchuk.inf.stack;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ReversPolishNotation {
    private StringBuilder stringAnswer = new StringBuilder();
    static Stack<ru.kpfu.itis.group905.Romanchuk.inf.stack.StackElement> stack = new Stack<>();
    static Stack<Double> stack2 = new Stack<>();

    public StringBuilder reformat(String string) {
        Set<String> operations = operations();

        if (string.equals("(")) {
//            stack.push();
        } else if (string.equals(")")) {
            while (!(stack.peek().equals("("))) {
                stringAnswer.append(stack.pop()).append(" ");
            }
            stack.pop();
        } else if (string.equals("0")) {
            while (!(stack.isEmpty())) {
                stringAnswer.append(stack.pop()).append(" ");
            }
        } else if (ReversPolishNotation.isDigit(string)) {
            stringAnswer.append(string).append(" ");
        } else if (operations.contains(string)) {
            Operation.operation(string, stack);
//            String temp = stack.peek();
//            if (temp != null) {
//                // такие сложные конструкции смотрятся плохо( понимаются еще хуже
//                if (!(string.equals("^")) & !(temp.equals("^")) & (temp.equals("*")
//                        | temp.equals("/") || ((temp.equals("+")
//                        || temp.equals("-")) & (string.equals("+") || string.equals("-"))))) {
//                    stringAnswer.append(stack.pop()).append(" ");
//                }
//            }
//            stack.push(string);
        }
        return stringAnswer;
    }

    private static boolean isDigit(String s) throws NumberFormatException {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    interface StackElement {
        int value();
    }

    static class Number implements StackElement {
        private int _value;

        @Override
        public int value() {
            return 0;
        }
    }

    public static Set<String> operations() {
        Set<String> operations = new HashSet<>();
        operations.add("+");
        operations.add("-");
        operations.add("*");
        operations.add("/");
        operations.add("^");

        return operations;
    }

    public static Double count(StringBuilder string) {
        Set<String> operations = operations();

        for (String temp : string.toString().split(" ")) {
            if (operations.contains(temp)) {
                double x = stack2.pop();
                double y = stack2.pop();
//                Operation.operation(temp, stack);
            } else {
                stack2.push(Double.parseDouble(temp));
            }
        }
        return stack2.peek();
    }

    public static void main(String[] args) {
        ReversPolishNotation revers = new ReversPolishNotation();
        Scanner sc = new Scanner(System.in);

        String string = sc.next();
        while (!string.equals("0")) {
            System.out.println(revers.reformat(string));
            string = sc.next();
        }

        StringBuilder answer = revers.reformat("0");
        System.out.println("Наша польская запись: " + answer);
        System.out.println(ReversPolishNotation.count(answer));
    }
}
