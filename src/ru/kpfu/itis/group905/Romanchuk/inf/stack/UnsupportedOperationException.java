package ru.kpfu.itis.group905.Romanchuk.inf.stack;

public class UnsupportedOperationException extends IllegalStateException {
    public UnsupportedOperationException(String message) {
        super(message);
    }
}
