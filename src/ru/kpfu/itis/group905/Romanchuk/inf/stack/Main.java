package ru.kpfu.itis.group905.Romanchuk.inf.stack;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        run();
    }

    // зачем этот метод?
    private static void run() {
        Stack<Flat> stack = new Stack<>();

        try (Scanner scanner = new Scanner(new File("in.txt"))) {
            while (scanner.hasNext()) {
                int x = scanner.nextInt();
                double y = scanner.nextDouble();
                double z = scanner.nextDouble();
                y = y * z;
                stack.push(new Flat(x, y));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(stack);
        // у Flat должен был быть Cоmparable, и тогда эта строчка выраждается в Collections.sort()
        stack.sort((o1, o2) -> (int) (o1.getSquare() - o2.getSquare()));
        System.out.println(stack);
    }
}
