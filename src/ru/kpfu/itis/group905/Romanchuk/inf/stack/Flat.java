package ru.kpfu.itis.group905.Romanchuk.inf.stack;

public class Flat {
    private int name;
    private double square;

    public Flat(int x, double y) {
        this.name = x;
        this.square = y;
    }

    @Override
    public String toString() {
        return "Flat {" +
                "name = " + name +
                ", square = " + square +
                '}';
    }

    public double getSquare() {
        return square;
    }
}
