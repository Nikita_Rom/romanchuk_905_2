package ru.kpfu.itis.group905.Romanchuk.inf.stack;

public abstract class Operation implements StackElement {
    public static Object operation(String operation, Stack<StackElement> stack) {
        switch (operation) {
            case "+":
                stack.push(SumOperation.sumOperation(stack.pop(), stack.pop()));
                break;
            case "-":
                stack.push(DedOperation.dedOperation(stack.pop(), stack.pop()));
            break;
            case "/":
                stack.push(DivOperation.divOperation(stack.pop(), stack.pop()));
            break;
            case "*":
                stack.push(MultOperation.multOperation(stack.pop(), stack.pop()));
            break;
            case "^":
                stack.push(DegOperation.degOperation(stack.pop(), stack.pop()));
            break;
        }
        return null;
    }

    public abstract int value();

    public abstract boolean isPushNeeded(Stack<Operation> stack);

}
