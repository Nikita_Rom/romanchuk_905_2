package ru.kpfu.itis.group905.Romanchuk.inf.stack;

public class Node<T> {
    private T value;
    private Node<T> previous;

    public Node(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public Node<T> getPrevious() {
        return previous;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void setPrevious(Node<T> previous) {
        this.previous = previous;
    }
}
