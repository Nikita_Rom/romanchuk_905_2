package ru.kpfu.itis.group905.Romanchuk.inf.stack;

import java.util.*;
import java.util.function.UnaryOperator;

public class Stack<T> implements List<T> {
    private int size;
    private Node<T> up;

    public Stack() {
        up = null;
        size = 0;
    }

    public void push(T str) {
        Node<T> temp = new Node<>(str);
        if (size != 0) {
            temp.setPrevious(up);
        }

        up = temp;
        size++;
    }

    public T pop() {
        size--;
        Node<T> temp = up;
        up = up.getPrevious();
        return temp.getValue();
    }

    // ToDo: обратите внимание на то, как я ставлю отступы
    //  они нужны чтобы визуально разделить логические блоки
    public T peek() {
        if (size == 0) {
            return null;
        }

        return up.getValue();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("НЕЛЬЗЯ");
    }

    @Override
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException("НЕЛЬЗЯ");
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        for (int i = size - 1; i >= 0; i--) {
            array[i] = this.pop();
        }

        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("НЕЛЬЗЯ");
    }

    @Override
    public boolean add(T t) {
        throw new UnsupportedOperationException("НЕЛЬЗЯ");
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("НЕЛЬЗЯ");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("НЕЛЬЗЯ");
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        throw new UnsupportedOperationException("НЕЛЬЗЯ");
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("НЕЛЬЗЯ");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("НЕЛЬЗЯ");
    }

    // в методах без тела фигурные скобки можно оставить на одной строке
    @Override
    public void replaceAll(UnaryOperator<T> operator) {}

    @Override
    public void clear() {
        throw new UnsupportedOperationException("НЕЛЬЗЯ");
    }

    @Override
    public T get(int index) {
        return null;
    }

    @Override
    public T set(int index, T element) {
        return null;
    }

    @Override
    public void add(int index, T element) {}

    @Override
    public T remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

    // раз он вам понадобился, значит вы что-то сделали не так
    @Override
    public void sort(Comparator<? super T> c) {
        Object[] a = this.toArray();
        Arrays.sort(a, (Comparator) c);
        for (int i = 0; i < a.length; i++) {
            this.push((T) a[i]);
        }
    }

    @Override
    public String toString() {
        return "Stack {" +
                "size = " + size +
                ", up = " + up.getValue() +
                '}';
    }
}
