package ru.kpfu.itis.group905.Romanchuk.inf.stack;

public class SumOperation {
    private static double sum;

    static StackElement sumOperation(StackElement a, StackElement b) {
        sum = Double.parseDouble(String.valueOf(a)) + Double.parseDouble(String.valueOf(b));
//        StackElement.setValue(sum);
        return StackElement.getValue();
    }

//    public String toString() {
//        return String.valueOf(sum);
//    }

//    public boolean isPushNeeded(Stack<Operation> stack) {
//        return !stack.isEmpty() && this.priority < stack.peek().priority;
//    }
}
