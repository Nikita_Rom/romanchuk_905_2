package ru.kpfu.itis.group905.Romanchuk.inf.files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class JavaFile {

    public static void main(String[] args) throws IOException {

        List<File> string = (Files.walk(Paths.get("src/ru/kpfu/itis/group905/Romanchuk/"))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList())
        );

        Map<Long, File> map = new TreeMap<>();

        int n = 0;
        for (File e : string) {
            String str = e.toString();
            if (str.matches("^.*\\.java$")) {
                n++;
                long date = e.lastModified();
                map.put(date, e);
            }
        }

        for (long key : map.keySet()) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            System.out.println(sdf.format(new Date(key)) + " " + map.get(key));
        }

        System.out.println("there were created: " + n + " files");
    }
}
