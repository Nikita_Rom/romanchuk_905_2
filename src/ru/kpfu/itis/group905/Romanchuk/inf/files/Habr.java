package ru.kpfu.itis.group905.Romanchuk.inf.files;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Habr {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://habr.com/ru/");
        URLConnection connection = url.openConnection();

        FileOutputStream fos = new FileOutputStream("output_Habr.txt");

        int c = 0;
        InputStream urlStream = connection.getInputStream();
        String htmlString = "";
        String htmlString2 = "";
        while ((c = urlStream.read()) != -1) {
            if ((char) c != '\n') {
                htmlString += (char) c;
                htmlString2 += (char) c;
            } else {
                Pattern p = Pattern.compile(
                        "<a href=\"(https://habr\\.com/ru/post/\\d+/)\" class=\"post__title_link\">(.+)</a>");
                Matcher m = p.matcher(htmlString.trim());
                if (m.matches()) {
                    System.out.println(m.group(1) + " " + m.group(2));
                }

                Pattern p2 = Pattern.compile(
                        "<div class=\"post__text post__text-html {2}post__text_v1 \">(.+)</div>");
                // <div class="post__text post__text-html  post__text_v1 ">
                Matcher m2 = p2.matcher(htmlString2.trim());
                if (m2.matches()) {
                    System.out.println("\n" + m2.group(1));
                }

                try {
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(m);
                    oos.writeObject(m2);
                }
                catch(Exception e){
                    System.out.println(e.getMessage());
                }

                htmlString = ""; htmlString2 = "";
            }

        }
    }
}
