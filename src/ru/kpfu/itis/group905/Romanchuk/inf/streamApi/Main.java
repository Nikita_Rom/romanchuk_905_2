package ru.kpfu.itis.group905.Romanchuk.inf.streamApi;

/**
 @author Nikita Romanchuk
 11-905
 streamApi
*/

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static java.util.Arrays.sort;

public class Main {
    Map<String, ArrayList<String>> friends = new HashMap<>();

    public static void main(String[] args) throws IOException {
        Main main = new Main();
        main.read();
    }

    private void read() throws IOException {
        FileReader id = new FileReader("src\\ru\\kpfu\\itis\\group905\\Romanchuk" +
                "\\inf\\streamApi\\users.csv");
        Scanner scanner = new Scanner(id);

        int COUNT = 500;

        Person[] person = new Person[COUNT + 1];

        for (int i = 1; i <= COUNT; i++) {
            int idTemp;
            String nameTemp;
            String cityTemp;

            String temp = scanner.nextLine();

            String[] temp2;
            temp2 = temp.split(" ");
            idTemp = Integer.parseInt(temp2[0]);
            nameTemp = temp2[1] + " " + temp2[2] + " " + temp2[3];
            cityTemp = temp2[4];
            person[i] = new Person(idTemp, nameTemp, cityTemp);
        }
        id.close();

        FileReader id2 = new FileReader("src\\ru\\kpfu\\itis\\group905\\Romanchuk" +
                "\\inf\\streamApi\\subscriptions.csv");
        Scanner sc = new Scanner(id2);
        HashMap<Integer, int[]> mapSub = new HashMap<>();
        for (int i = 1; i <= COUNT; i++) {
            String temp = sc.nextLine();
            String[] temp2 = temp.split("\\s");
            int[] sub = new int[temp2.length - 1];
            for (int j = 0; j < temp2.length - 1; j++) {
                sub[j] = Integer.parseInt(temp2[j + 1]);
            }
            sort(sub);
            mapSub.put(i, sub);
        }
        id2.close();

        for (int i = 1; i <= COUNT; i++) {
            boolean fr = false;
            ArrayList<String> listFriend = new ArrayList<>();
            for (int j = 1; j <= mapSub.get(i).length; j++) {
                if (Arrays.binarySearch(mapSub.get(mapSub.get(i)[j - 1]), i) >= 0) {
                    fr = true;
                    listFriend.add(person[mapSub.get(i)[j - 1]].getName());
                }
            }
            if (fr) friends.put(person[i].getName(), listFriend);
        }

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 1; i <= COUNT; i++) {
            boolean sameSity = true;
            String temp = person[mapSub.get(i)[1]].getCity();
            for (int j = 1; j <= mapSub.get(i).length; j++) {
                if (!person[mapSub.get(i)[j - 1]].getCity().equals(temp)) {
                    sameSity = false;
                    break;
                }
            }
            if (sameSity) {
                strBuilder.append(person[i].getName()).append(" подписан(а) на жителей города ")
                        .append(temp).append("\n");
            }
        }
        if (strBuilder.length() != 0) {
            write2(strBuilder);
        }

        write(person, COUNT);
    }

    public void write(Person[] person, int count) throws IOException {
        FileWriter res = new FileWriter("src\\ru\\kpfu\\itis\\group905\\Romanchuk" +
                "\\inf\\streamApi\\output.txt");

        for (int i = 1; i <= count; i++) {
            res.write(person[i].toString());
        }
        res.close();

        FileWriter res2 = new FileWriter("src\\ru\\kpfu\\itis\\group905\\Romanchuk" +
                "\\inf\\streamApi\\friend.txt");
        for (String key : friends.keySet()) {
            res2.write(key + ", друзья: " +  friends.get(key) + "\n");
        }
        res2.close();
    }

    public void write2(StringBuilder str) throws IOException {
        FileWriter res = new FileWriter("src\\ru\\kpfu\\itis\\group905\\Romanchuk" +
                "\\inf\\streamApi\\subOnPeopleFromOneCity.txt");
        res.write(String.valueOf(str));
        res.close();
    }
}
