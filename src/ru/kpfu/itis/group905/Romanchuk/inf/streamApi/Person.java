package ru.kpfu.itis.group905.Romanchuk.inf.streamApi;

/**
 * @author Nikita Romanchuk
 * 11-905
 * streamApi_Person
 */

public class Person {
    int id; String name; String city;

    public Person(int id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "ID = " + id +
                ", name: " + name +
                ", city: " + city + "\n";
    }
}
