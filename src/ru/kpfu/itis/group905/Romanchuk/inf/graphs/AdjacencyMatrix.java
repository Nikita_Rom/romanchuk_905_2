package ru.kpfu.itis.group905.Romanchuk.inf.graphs;

import java.util.ArrayList;

public class AdjacencyMatrix implements Graph {
    private ArrayList<ArrayList<Integer>> matrix;
    private ArrayList<Vertex> vertices;

    public AdjacencyMatrix() {
        matrix = new ArrayList<>();
        vertices = new ArrayList<>();
    }

    public ArrayList<ArrayList<Integer>> getMatrix() {
        return matrix;
    }

    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    @Override
    public void add(Edge edge) {
        Vertex v1 = edge.getStart();
        Vertex v2 = edge.getEnd();
        if (!vertices.contains(v1)) {
            addVertex(v1);
        }
        if (!vertices.contains(v2)) {
            addVertex(v2);
        }
        int i1 = vertices.indexOf(v1);
        int i2 = vertices.indexOf(v2);
        matrix.get(i1).set(i2, 1);
        matrix.get(i2).set(i1, 1);
    }

    private void addVertex(Vertex vertex) {
        vertices.add(vertex);
        int i = vertices.indexOf(vertex);
        matrix.add(new ArrayList<>());
        for (int j = 0; j < vertices.size() - 1; j++) {
            matrix.get(i).add(0);
        }
        for (ArrayList<Integer> e : matrix
        ) {
            e.add(0);
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Список вершин: " + vertices + "\n");
        stringBuilder.append("AdjacencyMatrix: \n");
        for (ArrayList<Integer> row : matrix
        ) {
            for (Integer column : row
            ) {
                stringBuilder.append(column + " ");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
