package ru.kpfu.itis.group905.Romanchuk.inf.graphs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ListOfEdges listOfEdges = new ListOfEdges();
        create(listOfEdges);

        AdjacencyList adjacencyList = new AdjacencyList();
        create(adjacencyList);

        AdjacencyMatrix adjacencyMatrix = new AdjacencyMatrix();
        create(adjacencyMatrix);

        IncidenceMatrix incidenceMatrix = new IncidenceMatrix();
        create(incidenceMatrix);

        listOfEdgesFromAdjacencyList(adjacencyList);
        listOfEdgesFromIncidenceMatrix(incidenceMatrix);
        listOfEdgesFromAdjacencyMatrix(adjacencyMatrix);

        adjacencyMatrixFromAdjacencyList(adjacencyList);
        adjacencyMatrixFromIncidenceMatrix(incidenceMatrix);
        adjacencyMatrixFromListOfEdges(listOfEdges);

        adjacencyListFromIncidenceMatrix(incidenceMatrix);
        adjacencyListFromAdjacencyMatrix(adjacencyMatrix);
        adjacencyListFromListOfEdges(listOfEdges);

        incidenceMatrixFromAdjacencyList(adjacencyList);
        incidenceMatrixFromAdjacencyMatrix(adjacencyMatrix);
        incidenceMatrixFromListOfEdges(listOfEdges);
    }

    public static void create(Graph graph) {
        File input = new File("C:\\Users\\User\\Desktop\\JAVA Albina\\romanchuk_905_2" +
                "\\src\\ru\\kpfu\\itis\\group905\\Romanchuk\\inf\\graphs\\input.txt");
        Scanner scanner;
        try {
            scanner = new Scanner(input);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }

        while (scanner.hasNextLine()) {
            Vertex v1 = new Vertex(scanner.nextInt());
            Vertex v2 = new Vertex(scanner.nextInt());
            graph.add(new Edge(v1, v2));
        }
        System.out.println(graph);
    }

    public static ListOfEdges listOfEdgesFromAdjacencyList(AdjacencyList adjacencyList) {
        ListOfEdges listOfEdges = new ListOfEdges();
        fromAdjacencyList(listOfEdges, adjacencyList);
        System.out.println("listOfEdgesFromAdjacencyList: ");
        System.out.println(listOfEdges);
        return listOfEdges;
    }

    public static ListOfEdges listOfEdgesFromIncidenceMatrix(IncidenceMatrix incidenceMatrix) {
        ListOfEdges listOfEdges = new ListOfEdges();
        fromIncidenceMatrix(listOfEdges, incidenceMatrix);
        System.out.println("listOfEdgesFromIncidenceMatrix: ");
        System.out.println(listOfEdges);
        return listOfEdges;
    }

    public static ListOfEdges listOfEdgesFromAdjacencyMatrix(AdjacencyMatrix adjacencyMatrix) {
        ListOfEdges listOfEdges = new ListOfEdges();
        fromAdjacencyMatrix(listOfEdges, adjacencyMatrix);
        System.out.println("listOfEdgesFromAdjacencyMatrix: ");
        System.out.println(listOfEdges);
        return listOfEdges;
    }

    public static AdjacencyMatrix adjacencyMatrixFromIncidenceMatrix(IncidenceMatrix incidenceMatrix) {
        AdjacencyMatrix adjacencyMatrix = new AdjacencyMatrix();
        fromIncidenceMatrix(adjacencyMatrix, incidenceMatrix);
        System.out.println("adjacencyMatrixFromIncidenceMatrix: ");
        System.out.println(adjacencyMatrix);
        return adjacencyMatrix;
    }

    public static AdjacencyMatrix adjacencyMatrixFromListOfEdges(ListOfEdges listOfEdges) {
        AdjacencyMatrix adjacencyMatrix = new AdjacencyMatrix();
        fromListOfEdges(adjacencyMatrix, listOfEdges);
        System.out.println("adjacencyMatrixFromListOfEdges: ");
        System.out.println(adjacencyMatrix);
        return adjacencyMatrix;
    }

    public static AdjacencyMatrix adjacencyMatrixFromAdjacencyList(AdjacencyList adjacencyList) {
        AdjacencyMatrix adjacencyMatrix = new AdjacencyMatrix();
        fromAdjacencyList(adjacencyMatrix, adjacencyList);
        System.out.println("adjacencyMatrixFromAdjacencyList: ");
        System.out.println(adjacencyMatrix);
        return adjacencyMatrix;
    }

    public static AdjacencyList adjacencyListFromListOfEdges(ListOfEdges listOfEdges) {
        AdjacencyList adjacencyList = new AdjacencyList();
        fromListOfEdges(adjacencyList, listOfEdges);
        System.out.println("adjacencyListFromListOfEdges: ");
        System.out.println(adjacencyList);
        return adjacencyList;
    }

    public static AdjacencyList adjacencyListFromAdjacencyMatrix(AdjacencyMatrix adjacencyMatrix) {
        AdjacencyList adjacencyList = new AdjacencyList();
        fromAdjacencyMatrix(adjacencyList, adjacencyMatrix);
        System.out.println("adjacencyListFromListOfEdges: ");
        System.out.println(adjacencyList);
        return adjacencyList;
    }

    public static AdjacencyList adjacencyListFromIncidenceMatrix(IncidenceMatrix incidenceMatrix) {
        AdjacencyList adjacencyList = new AdjacencyList();
        fromIncidenceMatrix(adjacencyList, incidenceMatrix);
        System.out.println("adjacencyListFromListOfEdges: ");
        System.out.println(adjacencyList);
        return adjacencyList;
    }

    public static IncidenceMatrix incidenceMatrixFromListOfEdges(ListOfEdges listOfEdges) {
        IncidenceMatrix incidenceMatrix = new IncidenceMatrix();
        fromListOfEdges(incidenceMatrix, listOfEdges);
        System.out.println("incidenceMatrixFromListOfEdges: ");
        System.out.println(incidenceMatrix);
        return incidenceMatrix;
    }

    public static IncidenceMatrix incidenceMatrixFromAdjacencyMatrix(AdjacencyMatrix adjacencyMatrix) {
        IncidenceMatrix incidenceMatrix = new IncidenceMatrix();
        fromAdjacencyMatrix(incidenceMatrix, adjacencyMatrix);
        System.out.println("incidenceMatrixFromAdjacencyMatrix: ");
        System.out.println(incidenceMatrix);
        return incidenceMatrix;
    }

    public static IncidenceMatrix incidenceMatrixFromAdjacencyList(AdjacencyList adjacencyList) {
        IncidenceMatrix incidenceMatrix = new IncidenceMatrix();
        fromAdjacencyList(incidenceMatrix, adjacencyList);
        System.out.println("incidenceMatrixFromAdjacencyList: ");
        System.out.println(incidenceMatrix);
        return incidenceMatrix;
    }

    public static void fromIncidenceMatrix(Graph graph, IncidenceMatrix incidenceMatrix) {
        for (Edge edge : incidenceMatrix.getEdges()
        ) {
            graph.add(edge);
        }
    }


    public static void fromListOfEdges(Graph graph, ListOfEdges listOfEdges) {
        for (Edge edge : listOfEdges.getEdges()
        ) {
            graph.add(edge);
        }
    }

    public static void fromAdjacencyMatrix(Graph graph, AdjacencyMatrix adjacencyMatrix) {
        ArrayList<Vertex> vertices = adjacencyMatrix.getVertices();
        ArrayList<ArrayList<Integer>> matrix = adjacencyMatrix.getMatrix();
        for (int i = 0; i < vertices.size(); i++) {
            for (int j = i; j < vertices.size(); j++) {
                if (matrix.get(i).get(j) != 0) {
                    graph.add(new Edge(vertices.get(i), vertices.get(j)));
                }
            }
        }
    }

    public static void fromAdjacencyList(Graph graph, AdjacencyList adjacencyList) {
        for (Map.Entry<Vertex, ArrayList<Edge>> entry : adjacencyList.getGraph().entrySet()
        ) {
            for (Edge edge : entry.getValue()
            ) {
                graph.add(edge);
            }
        }
    }
}
