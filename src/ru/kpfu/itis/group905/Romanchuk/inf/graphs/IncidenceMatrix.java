package ru.kpfu.itis.group905.Romanchuk.inf.graphs;

import java.util.ArrayList;

public class IncidenceMatrix implements Graph {
    private ArrayList<ArrayList<Integer>> matrix;
    private ArrayList<Vertex> vertices;
    private ArrayList<Edge> edges;

    public IncidenceMatrix() {
        matrix = new ArrayList<>();
        vertices = new ArrayList<>();
        edges = new ArrayList<>();
    }

    @Override
    public void add(Edge edge) {
        if (!edges.contains(edge)){
            edges.add(edge);
            Vertex v1 = edge.getStart();
            Vertex v2 = edge.getEnd();
            if(!vertices.contains(v1)){
                addVertex(v1);
            }
            if(!vertices.contains(v2)){
                addVertex(v2);
            }
            for (ArrayList<Integer> e : matrix
            ) {
                e.add(0);
            }
            int i1 = vertices.indexOf(v1);
            int i2 = vertices.indexOf(v2);
            int edgeIndex = edges.indexOf(edge);
            if (v1.equals(v2)){
                matrix.get(i1).set(edgeIndex,2);
            } else {
                matrix.get(i1).set(edgeIndex,1);
                matrix.get(i2).set(edgeIndex,1);
            }
        }
    }

    private void addVertex(Vertex v) {
        vertices.add(v);
        matrix.add(new ArrayList<>());
        int i = vertices.indexOf(v);
        for (int j = 0; j < edges.size() - 1 ; j++) {
            matrix.get(i).add(0);
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Список вершин: " + vertices + "\n");
        stringBuilder.append("Список ребер: " + edges + "\n");
        stringBuilder.append("IncidenceMatrix: \n");
        for (ArrayList<Integer> row : matrix
        ) {
            for (Integer column : row
            ) {
                stringBuilder.append(column + " ");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public ArrayList<ArrayList<Integer>> getMatrix() {
        return matrix;
    }

    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    public ArrayList<Edge> getEdges() {
        return edges;
    }
}
