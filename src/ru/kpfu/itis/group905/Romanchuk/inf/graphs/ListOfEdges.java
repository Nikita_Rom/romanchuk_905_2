package ru.kpfu.itis.group905.Romanchuk.inf.graphs;

import java.util.ArrayList;
import java.util.HashSet;

public class ListOfEdges implements Graph {
    private ArrayList<Edge> edges;
    private HashSet<Vertex> vertices;


    public ListOfEdges() {
        edges = new ArrayList<>();
        vertices = new HashSet<>();
    }

    @Override
    public void add(Edge edge) {
        if (!edges.contains(edge)) {
            vertices.add(edge.getStart());
            vertices.add(edge.getEnd());
            edges.add(edge);
        }
    }

    public ArrayList<Edge> getEdges() {
        return edges;
    }

    public HashSet<Vertex> getVertices() {
        return vertices;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Список ребер: \n");
        for (Edge e : edges
        ) {
            stringBuilder.append(e + "\n");
        }

        return stringBuilder.toString();
    }
}
