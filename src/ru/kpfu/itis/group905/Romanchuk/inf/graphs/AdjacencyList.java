package ru.kpfu.itis.group905.Romanchuk.inf.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdjacencyList implements Graph {
    private HashMap<Vertex, ArrayList<Edge>> graph;

    public AdjacencyList() {
        graph = new HashMap<>();
    }

    public HashMap<Vertex, ArrayList<Edge>> getGraph() {
        return graph;
    }

    @Override
    public void add(Edge edge) {
        if (edge.getStart().equals(edge.getEnd())){
            addEdge(edge.getStart(), edge);
        } else {
            addEdge(edge.getStart(), edge);
            addEdge(edge.getEnd(), edge);
        }

    }

    private void addEdge(Vertex v, Edge edge){
        if (graph.containsKey(v)){
            if (!graph.get(v).contains(edge)){
                graph.get(v).add(edge);
            }
        } else {
            graph.put(v, new ArrayList<>());
            graph.get(v).add(edge);
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("AdjacencyList: \n");
        for (Map.Entry<Vertex, ArrayList<Edge>> e:graph.entrySet()
        ) {
            stringBuilder.append(e.getKey() + ": ");
            for (Edge edge: e.getValue()
            ) {
                Vertex vertex = edge.getEnd();
                if (!vertex.equals(e.getKey())){
                    stringBuilder.append(vertex + " ");
                } else {
                    stringBuilder.append(edge.getStart() + " ");
                }
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
