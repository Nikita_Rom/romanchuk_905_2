package ru.kpfu.itis.group905.Romanchuk.inf.regex;

import java.util.Scanner;

/**
 * @author Nikita Romanchuk
 * 11-905
 * Task2
 */

public class Task2 {
    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        String str = sc.nextLine();
        Task2 task = new Task2();
        String str1 = "-0";
        String str2 = "9.(3)"; // не работает, но я пытался. Столько времени на скобки убил, но он все равно вредничает
        String str3 = "0.0";
        String str4 = "9,75";
        String str5 = "0.01";
        String str6 = "0.010";
        task.period(str1);
        task.period(str2);
        task.period(str3);
        task.period(str4);
        task.period(str5);
        task.period(str6);
    }

    public void period(String str) {
        boolean result = str.matches("^-?\\+?\\d[^\\-0]\\d+((((\\.,\\d)*[1-9])+)|((\\.,\\d)*([(]*\\d*[)]+)))$");

        System.out.println(result);
    }
}

//Task2. Напишите регулярное выражение для вещественного числа с периодом.
//Подходят: 0, -6, -0.5, +2, 0,0(64),
//Не подходят: -0, 001, 0,(35)00, -3,750