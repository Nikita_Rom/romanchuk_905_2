package ru.kpfu.itis.group905.Romanchuk.inf.regex;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nikita Romanchuk
 * 11-905
 * Task2
 */

public class Task3 {
    public static void main(String[] args) {
        Random random = new Random();
        int n = 0;
        int count = 0;
        int count2 = 0;

        System.out.println("matches:");

        while (n != 10) {
            long k = random.nextInt(2100000000);
            String str = String.valueOf(k);
            if (str.matches("^([13579]+[24680]{0,2}[13579]+)*$")) {
                n++;
                System.out.println(k);
            }
            count++;
        }
        System.out.println(count);

        n = 0;
        System.out.println("\nfind:");

        while (n != 10) {
            long k = random.nextInt(2100000000);
            String str = String.valueOf(k);
            Pattern pattern = Pattern.compile("([13579]*[24680]{3}[13579]*)");
            Matcher matcher = pattern.matcher(str);
            if (!matcher.find()) {
                n++;
                System.out.println(k);
            }
            count++;
        }
        System.out.println(count);
    }
}

// Task3. Генерировать случайные положительные целые числа c помощью java.util.Random.
// Вывести первые 10 сгенерированных чисел, в которых нет трех четных цифр подряд.
// Остановить генератор, вывести общее количество сгенерированных чисел.
// Проверку осуществлять регулярным выражением.
// НЕ использовать математические операции для анализа числа (сделать двумя способами – с помощью matches и с помощью find)