package ru.kpfu.itis.group905.Romanchuk.inf.regex;

/**
 * @author Nikita Romanchuk
 * 11-905
 * Task1
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        Task1 task = new Task1();
        if (task.date(str)) {
            System.out.println("Yeap, the date corresponds to our interval");
        } else System.out.println("No, the date doesn't match our interval");
    }

    public boolean date(String str) {
        int month, day, year;
        int hour, minute;
        String[] str1 = str.split(" ");
        String[] str2 = str1[1].split(":");
        hour = Integer.parseInt(str2[0]);
        minute = Integer.parseInt(str2[1]);
        String[] str3 = str1[0].split("/");
        month = Integer.parseInt(str3[0]);
        day = Integer.parseInt(str3[1]);
        year = Integer.parseInt(str3[2]);

        boolean visokos = false;
        if ((month == 2) && (day == 29)) {
            if ((year % 4 == 0) && (year % 100 != 0)) {
                visokos = true;
            } else if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0)) {
                visokos = true;
            }
        }

        Task1 task = new Task1();
        Map<Integer, Integer> mon = task.mapka();

        if (!(mon.containsKey(month))) {
            System.out.println("error entering month correctly");
            return false;
        } else if (day > mon.get(month) && !(visokos)) {
            System.out.println("error entering day correctly");
            return false;
        }
        if (hour > 23 || minute > 59) {
            System.out.println("error entering time correctly");
            return false;
        }


        if ((year <= 1978) && (year >= 1237)) {
            if ((year == 1237) && (month >= 3) && (day >= 6) && (hour >= 12)) {
                return true;
            } else if (year == 1237) return false;
            if ((year == 1978) && (month <= 2) && (day <= 27) && ((hour < 21)
                    || ((hour == 21) && minute <= 35))) {
                return true;
            } else if (year == 1978) return false;

            return true;
        }
        return false;
    }

    public Map<Integer, Integer> mapka() {
        Map<Integer, Integer> mon = new HashMap<>();
        mon.put(1, 31);
        mon.put(2, 28);
        mon.put(3, 31);
        mon.put(4, 30);
        mon.put(5, 31);
        mon.put(6, 30);
        mon.put(7, 31);
        mon.put(8, 31);
        mon.put(9, 30);
        mon.put(10, 31);
        mon.put(11, 30);
        mon.put(12, 31);

        return mon;
    }

    /*
    Task1. Напишите регулярное выражения для даты и времени в промежутке
    с 6 марта 1237 12:00 по 27 февраля 1978 21:35 в формате MM/DD/YYYY HH:MM
     */
}
