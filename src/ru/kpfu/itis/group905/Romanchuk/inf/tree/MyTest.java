package ru.kpfu.itis.group905.Romanchuk.inf.tree;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

public class MyTest extends Assert {
    private IntegerBST integers;

    @Before
    public void before() {
        int[] a = {1, 5, 0, 2, 6, -9, -4, -3, 0, 7, 8};
        integers = new IntegerBST(a);
    }


    @Test
    public void testCount(){
        HashMap<Integer, Integer> actual = integers.count();
        HashMap<Integer, Integer> expected = new HashMap<>();
        expected.put(-1,3);
        expected.put(0,2);
        expected.put(1, 6);
        assertEquals(expected, actual);
    }

    @Test
    public void testFindMaxSum(){
        int actual = integers.findMaxSum();
        int expected = 15;
        assertEquals(expected, actual);
    }


}

