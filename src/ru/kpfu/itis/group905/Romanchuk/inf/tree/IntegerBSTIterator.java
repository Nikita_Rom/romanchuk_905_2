package ru.kpfu.itis.group905.Romanchuk.inf.tree;

import java.util.Iterator;
import java.util.Stack;

public class IntegerBSTIterator implements Iterator<Integer> {
    Stack<Node> stack;

    public IntegerBSTIterator(Node root) {
        stack = new Stack<>();
        while (root != null) {
            stack.push(root);
            root = root.getLeft();
        }
    }

    @Override
    public boolean hasNext() {
        return !stack.isEmpty();
    }

    @Override
    public Integer next() {
        Node node = stack.pop();
        Integer value = node.getValue();
        if (node.getRight() != null) {
            node = node.getRight();
            while (node != null) {
                stack.push(node);
                node = node.getLeft();
            }
        }
        return value;
    }
}
