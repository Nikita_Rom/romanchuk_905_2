package ru.kpfu.itis.group905.Romanchuk.inf.tree;

import java.util.*;

public class IntegerBST implements Iterable<Integer> {
    private Node root;
    private int size;

    public IntegerBST(int[] array) {
        selectionSort(array);
        this.root = insert(root, array);
        this.size = array.length;
    }

    private Node insert(Node root, int[] array) {
        if (array.length != 0) {
            int middle = array.length / 2;
            root = new Node(array[middle]);
            root.setLeft(insert(root.getLeft(), Arrays.copyOfRange(array, 0, middle)));
            root.setRight(insert(root.getRight(), Arrays.copyOfRange(array, middle + 1, array.length)));
        } else {
            root = null;
        }
        return root;
    }

    public void selectionSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minId = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minId = j;
                }
            }
            int temp = array[i];
            array[i] = min;
            array[minId] = temp;
        }
    }

    public boolean contains(int value) {
        return contains(root, value);
    }

    public boolean contains(Node root, int value) {
        if (root.getValue().equals(value)) {
            return true;
        } else if (value < root.getValue() && root.getLeft() != null) {
            return contains(root.getLeft(), value);
        } else if (root.getRight() != null) {
            return contains(root.getRight(), value);
        } else {
            return false;
        }
    }

    public Integer findMaxSum() {
        int sum = 0;
        Node current = root;
        while (current != null) {
            sum += current.getValue();
            current = current.getRight();
        }
        return sum;
    }

    public HashMap<Integer, Integer> count() {
        int positive = 0;
        int negative = 0;
        int zero = 0;

        for (Integer x : this) {
            if (x < 0) {
                negative++;
            } else if (x == 0) {
                zero++;
            } else {
                positive++;
            }
        }

        HashMap<Integer, Integer> result = new HashMap<>();
        result.put(-1, negative);
        result.put(0, zero);
        result.put(1, positive);

        return result;
    }

    @Override
    public Iterator iterator() {
        return new IntegerBSTIterator(root);
    }
}

