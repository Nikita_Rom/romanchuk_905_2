package ru.kpfu.itis.group905.Romanchuk.inf.tree;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        IntegerBST tree = new IntegerBST(array);
        System.out.println(tree.findMaxSum());

        for (Map.Entry<Integer, Integer> set : tree.count().entrySet()) {
            System.out.println(set.getKey() + " " + set.getValue());
        }
    }
}
