package ru.kpfu.itis.group905.Romanchuk.inf.tree;

public class Node{
    private Integer value;
    private Node left;
    private Node right;

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    Node(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

}
