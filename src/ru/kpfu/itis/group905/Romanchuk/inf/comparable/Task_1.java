package ru.kpfu.itis.group905.Romanchuk.inf.comparable;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Task_1 {
    public static void main(String[] args) throws IOException {
        String file = "in.txt";
        final String path = "C:\\Users\\User\\Desktop\\JAVA Albina\\romanchuk_905_2\\src\\ru\\kpfu\\itis\\group905\\Romanchuk\\inf\\comparable\\";
        BufferedReader inFile = new BufferedReader(new FileReader(path + file));

        int k = Integer.parseInt(inFile.readLine());
        ArrayList<String> ArrayList = new ArrayList<>();
        String line;
        while ((line = inFile.readLine()) != null) {
            if (line.length() >= k) {
                ArrayList.add(line);
            }
        }
        inFile.close();

        PrintWriter writer = new PrintWriter(path + "out.txt");

        for (int i = 0; i < k; i++) {
            final int count = i;
            Comparator<String> comparator = Comparator.comparing(obj -> obj.charAt(count));
            Collections.sort(ArrayList, comparator);
            writer.println(i);
            writer.println(String.valueOf(ArrayList));
        }
        writer.flush();
        writer.close();
    }
}
