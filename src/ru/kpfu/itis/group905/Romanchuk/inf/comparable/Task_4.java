package ru.kpfu.itis.group905.Romanchuk.inf.comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Task_4 {
    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        int k = str.nextInt();
        ArrayList<Integer> ArrayList = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            ArrayList.add(str.nextInt());
        }

        Comparator<Integer> comparator = Comparator.comparing(Task_4::reverse);
        Collections.sort(ArrayList, comparator);

        System.out.println(ArrayList);
    }

    private static int reverse(int x) {
        int number = 0;
        while (x < 0) {
            number = number * 10 + x % 10;
            x = x / 10;
        }
        return number;
    }
}
