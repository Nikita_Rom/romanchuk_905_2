package ru.kpfu.itis.group905.Romanchuk.inf.comparable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Task_2 {
    public static void main(String[] args) throws IOException {
        String file = "in.txt";
        final String path = "C:\\Users\\User\\Desktop\\JAVA Albina\\romanchuk_905_2\\src\\ru\\kpfu\\itis\\group905\\Romanchuk\\inf\\comparable\\";
        BufferedReader inFile = new BufferedReader(new FileReader(path + file));

        int k = Integer.parseInt(inFile.readLine());
        ArrayList<Integer> ArrayList = new ArrayList<>();
        int number;
        for (int i = 0; i < k; i++) {
            number = Integer.parseInt(inFile.readLine());
            ArrayList.add(number);
        }
        inFile.close();

        PrintWriter writer = new PrintWriter(path + "out.txt");
        Comparator<Integer> comparator = Comparator.comparing(Task_2::count);
        Collections.sort(ArrayList, comparator);

        writer.flush();
        writer.close();
    }

    public static int count(int x) {
        int sum = 0;
        while (x > 0) {
            x = x/10;
            sum++;
        }
        return sum;
    }
}
