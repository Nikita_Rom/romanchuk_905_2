package ru.kpfu.itis.group905.Romanchuk.inf.kr1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) throws IOException {
        String file = "in.txt";
        final String path = "src/ru/kpfu/itis/group905/Romanchuk/inf/kr1/";
        BufferedReader inFile = new BufferedReader(new FileReader(path + file));

        LinkedList<String> LinkedList = new LinkedList<>();
        String line;
        String string = inFile.readLine();
        while ((line = inFile.readLine()) != null) {
            for (String temp : string.split(" ")) {
                LinkedList.add(line);
            }
        }
        inFile.close();



        PrintWriter writer = new PrintWriter(path + "out.txt");
        int q = LinkedList.size();
        for (int i = 0; i < q; i++){
            writer.println(LinkedList.pop());
        }
        writer.close();

        PrintWriter writer2 = new PrintWriter(path + "out2.txt");
        Collections.sort(LinkedList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });

        for (String s : LinkedList) {
            writer2.println(s);
        }
        writer2.close();
    }
}
