package ru.kpfu.itis.group905.Romanchuk.inf.kr1;

public class StackIsEmptyException extends Exception {
    public StackIsEmptyException(String message) {
        super(message);
    }
}
