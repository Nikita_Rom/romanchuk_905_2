package ru.kpfu.itis.group905.Romanchuk.inf.kr1;

import java.util.LinkedList;

public class Stack<E> extends LinkedList<E> {
    LinkedList <E> linkedList = new LinkedList<>();

    public void push(E data) {
        linkedList.add(data);
    }

//    public E pop() {
//         if (linkedList.isEmpty()) {
//             try {
//                 throw new StackIsEmptyException("Ошибка, нащальника");
//             } catch (StackIsEmptyException e) {
//                 e.printStackTrace();
//             }
//         }
//         else {
//             return linkedList.unlinkLast();
//         }
//    }

    @Override
    public String toString() {
        return "Stack{" +
                "linkedList=" + linkedList +
                '}';
    }

    @Override
    public E[] toArray(Object[] a) {
        return (E[]) super.toArray(a);
    }

    @Override
    public int size() {
        return super.size();
    }

    @Override
    public void clear() {
        super.clear();
    }
}
