package ru.kpfu.itis.group905.Romanchuk.algorithms.pair3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Task_1 {
    public static void main(String[] args) throws IOException {
        String file = "input.txt";
        final String path = "src/ru/kpfu/itis/group905/Romanchuk/algorithms/pair3/";
        BufferedReader inFile = new BufferedReader(new FileReader(path + file));

        HashMap<Integer, Integer> HashMap = new HashMap<>();

        String line;
        while ((line = inFile.readLine()) != null) {
            int temp = Integer.parseInt(line);
            if (HashMap.containsKey(temp)) {
                HashMap.put(temp, HashMap.get(temp) + 1);
            }
            else {
                HashMap.put(temp, 1);
            }

        }
        inFile.close();

        int unicValue = HashMap.size();

        int s = 0;
        for (HashMap.Entry entry : HashMap.entrySet()) {
            s += (int) entry.getValue();
        }

        int elem = 0;
        for (HashMap.Entry entry : HashMap.entrySet()) {
            if ((int) entry.getValue() > (int) s/2) {
                elem = (int) entry.getKey();
                break;
            }
        }
        System.out.println("Unique elements in the array: " + unicValue + ".\nThe element that occurs" +
                " the most (+by condition) " + elem + ".");

    }
}
