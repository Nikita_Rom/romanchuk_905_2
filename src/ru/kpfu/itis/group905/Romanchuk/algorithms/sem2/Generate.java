package ru.kpfu.itis.group905.Romanchuk.algorithms.sem2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class Generate {
    private static int COUNT = 80;

    public int getCOUNT() {
        return COUNT;
    }

    public static void main(String[] args) throws IOException {
        PrintWriter res = new PrintWriter("src\\ru\\kpfu\\itis\\group905\\Romanchuk\\algorithms\\sem2\\in.txt");

        Random random = new Random();

        int length = random.nextInt(10000);
        length = length < 100  ? (length + 100) : length;

        for (int i = 0; i < COUNT; i++) {
            int[] a = new int[length];
            res.write(length + " ");
            for (int j = 0; j < length; j++) {
                a[j] = random.nextInt(100000);;
                res.write(a[j] + " ");
            }
            length = random.nextInt(10000);
            length = length < 100  ? (length + 100) : length;
            res.write("\n");
        }

        res.close();
    }
}
