package ru.kpfu.itis.group905.Romanchuk.algorithms.sem2;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

class CombSort {
    private static int getNextGap(int gap) {
        gap = (gap * 10) / 13;
        if (gap < 1)
            return 1;
        return gap;
    }

    private static int sort(int[] arr) {
        int n = arr.length;
        int gap = n;

        boolean swapped = true;
        int k = 0;

        while (gap != 1 || swapped) {

            gap = getNextGap(gap);

            swapped = false;

            for (int i = 0; i < n - gap; i++) {
                if (arr[i] > arr[i + gap]) {
                    int temp = arr[i];
                    arr[i] = arr[i + gap];
                    arr[i + gap] = temp;
                    k++;
                    swapped = true;
                }
            }
        }
        return k;
    }


    private static int sortLinkedList(LinkedList<Integer> list) {
        int n = list.size();
        int gap = n;

        boolean swapped = true;
        int k = 0;

        while (gap != 1 || swapped) {

            gap = getNextGap(gap);

            swapped = false;

            for (int i = 0; i < n - gap; i++) {
                if (list.get(i) > list.get(i + gap)) {
                    int temp = list.get(i);
                    list.set(i, list.get(i + gap));
                    list.set(i + gap, temp);
                    k++;
                    swapped = true;
                }
            }
        }
        return k;
    }


    public static void main(String[] args) throws IOException {
        FileReader id = new FileReader("src/ru/kpfu/itis/group905/Romanchuk/algorithms/sem2/in.txt");
        FileWriter res = new FileWriter("src/ru/kpfu/itis/group905/Romanchuk/algorithms/sem2/out.txt");
        FileWriter res2 = new FileWriter("src/ru/kpfu/itis/group905/" +
                "Romanchuk/algorithms/sem2/outList.txt");
        Scanner sc = new Scanner(id);

        Generate generate = new Generate();
        int COUNT = generate.getCOUNT();

        for (int i = 0; i < COUNT; i++) {
            int n = sc.nextInt();
            int[] a = new int[n];
            LinkedList<Integer> list = new LinkedList<>();
            for (int j = 0; j < n; j++) {
                int temp = sc.nextInt();
                a[j] = temp;
                list.add(temp);
            }
            int k;
            int list2;

            long startTime = System.currentTimeMillis();
            long startTime2 = System.nanoTime();
            k = sort(a);
            long finishTime2 = System.nanoTime();
            long finishTime = System.currentTimeMillis();

            res.write("\nSorted array " + (i + 1) + ": ");
            res.write("\ncount of operation: " + k);
            res.write("\n" + (finishTime - startTime) + "ms " + (finishTime2 - startTime2) + " ns \n");
            for (int value : a) res.write(value + " ");


            long startTime4 = System.currentTimeMillis();
            long startTime3 = System.nanoTime();
            list2 = sortLinkedList(list);
            long finishTime3 = System.nanoTime();
            long finishTime4 = System.currentTimeMillis();

            res2.write("\nSorted LL " + (i + 1) + ": ");
            res2.write("\ncount of operation: " + list2);
            res2.write("\n" + (finishTime4 - startTime4) + "ms " + (finishTime3 - startTime3) + " ns \n");
            for (int value : list) res2.write(value + " ");

        }
        id.close();
        res.close();
        res2.close();
    }
}
