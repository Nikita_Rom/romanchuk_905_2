package ru.kpfu.itis.group905.Romanchuk.algorithms.codeforces3;

import java.util.Scanner;

public class Susi {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.next();
        String str2 = sc.next();
        int count = 0;

        for (int i = 0; i < str1.length(); i++) {
            if (str1.charAt(i) != str2.charAt(i)) count++;
        }
        if (count % 2 == 0) {
            int i = 0;
            count = count / 2;
            StringBuilder str = new StringBuilder();
            while (i != str1.length()) {
                if (str1.charAt(i) != str2.charAt(i) && count != 0) {
                    str.append(str2.charAt(i));
                    count--;
                } else str.append(str1.charAt(i));
                i++;
            }
            System.out.println(str);
        } else System.out.println("impossible");

    }
}
