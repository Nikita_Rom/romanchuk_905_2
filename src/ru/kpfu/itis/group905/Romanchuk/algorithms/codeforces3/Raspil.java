package ru.kpfu.itis.group905.Romanchuk.algorithms.codeforces3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Raspil {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int B = scanner.nextInt();
        ArrayList<Integer> bitkoins = new ArrayList<>();
        int even = 0;
        int uneven = 0;
        int x = 0;
        for (int i = 0; i < n; i++) {
            int y = scanner.nextInt();
            if (even == uneven && even != 0) {
                bitkoins.add(Math.abs(x - y));
            }
            x = y;
            if (x % 2 == 0) {
                even++;
            } else {
                uneven++;
            }
        }
        Collections.sort(bitkoins);
        int sum = 0;
        int count = 0;
        if (!bitkoins.isEmpty()) {
            int a = bitkoins.get(0);
            while (sum + a <= B && !bitkoins.isEmpty()) {
                count++;
                sum += a;
                bitkoins.remove(0);
                if (!bitkoins.isEmpty()) a = bitkoins.get(0);
            }
        }
        System.out.println(count);
    }
}