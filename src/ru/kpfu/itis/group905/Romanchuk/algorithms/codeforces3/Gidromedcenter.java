package ru.kpfu.itis.group905.Romanchuk.algorithms.codeforces3;

import java.util.Scanner;

public class Gidromedcenter {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = 0;
        int max = 0;
        for (int i = 0; i < n; i++) {
            int temp = sc.nextInt();
            if ((temp % 2 == 0) && (temp < 0)) {
                k++;
            } else k = 0;
            if (k > max) max = k;
        }
        System.out.println(max);
    }
}
