package ru.kpfu.itis.group905.Romanchuk.algorithms.codeforces3;

import java.util.Scanner;

import static java.util.Arrays.sort;

public class Doska {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String k = scanner.next();
        int[] a = new int[k.length()];
        int sum = 0;

        for (int i = 0; i < k.length(); i++) {
            a[i] = Integer.parseInt(String.valueOf(k.charAt(i)));
            sum += a[i];
        }
        if (sum < n) {
            sort(a);
            int o = 0;
            for (int i = 0; i < k.length(); i++) {
                sum = sum + (9 - a[i]);
                a[i] = 9;
                o++;
                if (sum >= n) {
                    System.out.println(o);
                    return;
                };
            }
        }
        System.out.println(0);
    }
}
