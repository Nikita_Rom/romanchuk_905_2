package ru.kpfu.itis.group905.Romanchuk.algorithms.codeforces3;

import java.util.Scanner;
import java.util.TreeMap;

public class Perestanovka {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int sum = 0;
        TreeMap<Integer, Integer> map = new TreeMap<>();
        for (int i = 0; i < n; i++) {
            int temp = sc.nextInt();
            map.put(temp, map.getOrDefault(temp, 0) + 1);
        }
        for (Integer key : map.keySet()) {

            if (map.get(key) > 1) {
                sum += map.get(key) - 1;
            }
            if (key > n) {
                sum += 1;
            }
        }
        System.out.println(sum);
    }
}
