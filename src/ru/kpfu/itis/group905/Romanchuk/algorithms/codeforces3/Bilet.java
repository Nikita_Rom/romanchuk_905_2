package ru.kpfu.itis.group905.Romanchuk.algorithms.codeforces3;

import java.util.Scanner;

import static java.util.Arrays.sort;

public class Bilet {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        int[] b = new int[n];
        String s = scanner.next();
        for (int i = 0; i < n; i++) {
            a[i] = s.charAt(i);
        }
        sort(a);
        for (int i = 0; i < n; i++) {
            b[i] = s.charAt(n + i);
        }
        sort(b);
        int bol = 0;
        int men = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] < b[i]) men++;
            if (a[i] > b[i]) bol++;
        }

        if (men == n || bol == n) System.out.println("YES");
        else System.out.println("NO");
    }
}
