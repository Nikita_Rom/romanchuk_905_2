package ru.kpfu.itis.group905.Romanchuk.algorithms.codeforces3;

import java.util.Scanner;

public class Munhen {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int min = 0;

        for (int i = 0; i < n; i++) {
            int temp = scanner.nextInt();
            for (int j = 1; j < m; j++) {
                int t = scanner.nextInt();
                if (t < temp) temp = t;
            }
            if (min < temp) min = temp;
        }

        System.out.println(min);
    }
}
