package ru.kpfu.itis.group905.Romanchuk.algorithms.codeforces3;

import java.util.Scanner;

public class Razbienie {

    private int[] a = new int[50];
    private static int count = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int terms = scanner.nextInt();
        Razbienie bity = new Razbienie();
        bity.rekursia(0, n, n, terms);
        System.out.print(count);
    }

    private void rekursia(int pos, int max, int num, int terms) {
        if (num != 0) {
            for (int i = 1; i <= Math.min(max, num); i++) {
                a[pos] = i;
                rekursia(pos + 1, i, num - i, terms);
            }
        } else if (pos <= terms) count++;
    }
}
