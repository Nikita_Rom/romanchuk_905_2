package ru.kpfu.itis.group905.Romanchuk.algorithms.sem1;

public class Elem {
    private int index;
    private int Value;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getValue() {
        return Value;
    }

    public void setValue(int value) {
        Value = value;
    }

    public Elem(int index, int value) {
        this.index = index;
        Value = value;
    }
}
