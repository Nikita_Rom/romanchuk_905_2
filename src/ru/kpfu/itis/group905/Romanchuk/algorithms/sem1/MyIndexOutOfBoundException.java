package ru.kpfu.itis.group905.Romanchuk.algorithms.sem1;

public class MyIndexOutOfBoundException extends Exception {
    public MyIndexOutOfBoundException(String message) {
        super(message);
    }
}
