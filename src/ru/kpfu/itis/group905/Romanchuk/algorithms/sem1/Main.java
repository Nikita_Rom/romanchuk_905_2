package ru.kpfu.itis.group905.Romanchuk.algorithms.sem1;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] a = {1, 5, 0, 1, 2, 0, 66, 0, 2, 0};
        int[] b = {0, 5, 1, 0, 0, 0, 6, 4, 3, 0};
        System.out.println("Массив 1: " + Arrays.toString(a));
        System.out.println("Массив 2: " + Arrays.toString(b));
        VectorCode vectorCode = new VectorCode(a);
        VectorCode vectorCode2 = new VectorCode(b);
        System.out.println("Связный список 1: " + vectorCode);
        System.out.println("Связный список 2: " + vectorCode2);

        System.out.println("decode(): " + Arrays.toString(vectorCode.decode()));

        try {
            VectorCode sum = vectorCode.sum(vectorCode2);
            System.out.println("sum(): " + sum);
            System.out.println("sum() array: " + Arrays.toString(sum.decode()));
        } catch (MyIndexOutOfBoundException e) {
            throw new IllegalStateException(e);
        }

        try {
            System.out.println("scalarProduct(): " + vectorCode.scalarProduct(vectorCode2));
        } catch (MyIndexOutOfBoundException e) {
            throw new IllegalStateException(e);
        }

        VectorCode vectorSum = vectorCode.vectorSum();
        System.out.println("vectorSum(): " + vectorSum);
        System.out.println("vectorSum() array: " + Arrays.toString(vectorSum.decode()));

        try {
            vectorCode.insert(10, 8);
        } catch (MyIndexOutOfBoundException e) {
            throw new IllegalStateException(e);
        }
        System.out.println("insert(10,8): " + vectorCode);
        vectorCode.mult(1, 4);
        System.out.println("mult(1,4): " + vectorCode);

        vectorCode.delete(1);
        System.out.println("delete(1): " + vectorCode);
    }
}
