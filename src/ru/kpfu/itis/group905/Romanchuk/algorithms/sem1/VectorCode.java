package ru.kpfu.itis.group905.Romanchuk.algorithms.sem1;

public class VectorCode {
    private int length;
    private MyLinkedList<Elem> list;

    public VectorCode(int[] arr) {
        this.length = arr.length;
        list = new MyLinkedList<>();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0) {
                list.add(new Elem(i, arr[i]));
            }
        }
    }

    public int[] decode() {
        int[] array = new int[length];
        for (Elem e : list) {
            array[e.getIndex()] = e.getValue();
        }
        return array;
    }

    public void mult(int a, int c) {
        for (Elem e : list) {
            if (e.getValue() == a) {
                e.setValue(a * c);
            }
        }
    }

    public VectorCode vectorSum() {
        int[] a = new int[length];
        VectorCode result = new VectorCode(a);
        int prev = 0;
        int j = 0;
        for (int i = 0; i < length; i++) {
            Elem x = j < this.getList().getSize() ? this.getList().get(j) : null;
            if ((x != null) && (x.getIndex() == i)) {
                result.getList().add(new Elem(i, x.getValue() + prev));
                prev = x.getValue();
                j++;
            } else {
                if (prev != 0) {
                    result.getList().add(new Elem(i, prev));
                }
                prev = 0;
            }
        }
        return result;
    }

    public VectorCode sum(VectorCode v) throws MyIndexOutOfBoundException {
        int[] array = new int[length];
        VectorCode result = new VectorCode(array);
        int a = 0;
        int b = 0;
        for (int i = 0; i < length; i++) {
            Elem x = a < this.getList().getSize() ? this.getList().get(a) : null;
            Elem y = b < v.getList().getSize() ? v.getList().get(b) : null;
            if ((x != null) && (y != null) && (x.getIndex() == i) && (y.getIndex() == i)) {
                a++;
                b++;
                result.insert(x.getValue() + y.getValue(), i);
            } else if ((x != null) && (x.getIndex() == i)) {
                a++;
                result.insert(x.getValue(), i);
            } else if ((y != null) && (y.getIndex() == i)) {
                b++;
                result.insert(y.getValue(), i);
            }
        }
        return result;
    }

    public int scalarProduct(VectorCode v) throws MyIndexOutOfBoundException {
        int a = 0;
        int b = 0;
        int sum = 0;
        for (int i = 0; i < length; i++) {
            Elem x = a < this.getList().getSize() ? this.getList().get(a) : null;
            Elem y = b < v.getList().getSize() ? v.getList().get(b) : null;
            if ((x != null) && (y != null) && (x.getIndex() == i) && (y.getIndex() == i)) {
                a++;
                b++;
                sum += x.getValue() * y.getValue();
            } else if ((x != null) && (x.getIndex() == i)) {
                a++;
            } else if ((y != null) && (y.getIndex() == i)) {
                b++;
            }
        }
        return sum;
    }

    public void delete(int pos) {
        for (Elem e : list) {
            if (pos == e.getIndex()) {
                e.setValue(0);
            }
        }
    }

    public void insert(int k, int pos) throws MyIndexOutOfBoundException {
        int i = 0;
        for (Elem e : list) {
            if (e.getIndex() == pos) {
                e.setValue(k);
                return;
            } else if (e.getIndex() > pos) {
                list.add(i, new Elem(pos, k));
                return;
            }
            i++;
        }
        list.add(i, new Elem(pos, k));

    }

    public MyLinkedList<Elem> getList() {
        return list;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (Elem e : list) {
            stringBuilder.append("(" + e.getIndex() + "," + e.getValue() + ") ");
        }
        return stringBuilder.toString();
    }

}
