package ru.kpfu.itis.group905.Romanchuk.algorithms.sem1;

import java.util.Iterator;

public class MyLinkedList<T> implements MyList<T>, Iterable<T> {
    private int size;
    private Node<T> head;
    private Node<T> tail;

    public MyLinkedList() {
        head = null;
        size = 0;
        tail = null;
    }


    public int getSize() {
        return size;
    }

    @Override
    public boolean add(T x) {
        if (size == 0) {
            head = new Node(x);
            tail = head;
        } else {
            Node temp = tail;
            tail = new Node(x);
            temp.setNext(tail);
        }
        size++;
        return true;
    }

    @Override
    public void add(int k, T x) throws MyIndexOutOfBoundException {
        if (k > size) {
            throw new MyIndexOutOfBoundException("Ошыбка нащальника");
        } else if (k == size) {
            add(x);
        } else {
            Node<T> current = head;
            for (int i = 1; i < k; i++) {
                current = current.getNext();
            }
            Node<T> temp = current.getNext();
            Node<T> novaya = new Node(x);
            novaya.setNext(temp);
            current.setNext(novaya);
            size++;
        }

    }

    @Override
    public void set(int k, T x) throws MyIndexOutOfBoundException {
        if (k >= size) {
            throw new MyIndexOutOfBoundException("Ошыбка нащальника [2]");
        } else {
            Node<T> current = head;
            for (int i = 1; i <= k; i++) {
                current = current.getNext();
                current.setValue(x);
            }
        }
    }

    @Override
    public boolean contains(T x) {
        Node<T> current = head;
        for (int i = 0; i < size; i++) {
            if (current.getValue().equals(x)) {
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

    @Override
    public int add(T[] a) {
        return 0;
    }

    @Override
    public int contains(T[] a) {
        return 0;
    }

    @Override
    public T get(int index) {
        Node<T> current = head;
        for (int i = 0; i < index; i++) {
            current = current.getNext();
        }
        return current.getValue();
    }

    @Override
    public Iterator<T> iterator() {
        return new MyLinkedListIterator<T>();
    }

    public class MyLinkedListIterator<E> implements Iterator<E> {
        private Node<E> currentNode;
        private int currentIndex;

        public MyLinkedListIterator() {
            this.currentNode = new Node<>(null);
            this.currentNode.setNext((Node<E>) head);
            this.currentIndex = -1;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < size - 1;
        }

        @Override
        public E next() {
            currentIndex++;
            currentNode = currentNode.getNext();
            return currentNode.getValue();
        }
    }
}