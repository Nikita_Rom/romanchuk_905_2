package ru.kpfu.itis.group905.Romanchuk.algorithms.sem1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MyTest extends Assert {
    private VectorCode vectorCode;

    @Before
    public void before(){
        int[] a = {1,5,0,1,2,0,66,0,2,0};
        vectorCode = new VectorCode(a);
    }


    @Test
    public void testScalarProduct() throws MyIndexOutOfBoundException {
        int[] b = {0,5,1,0,0,0, 6,4,3,0};
        VectorCode vectorCode2 = new VectorCode(b);
        int expected = 427;
        int actual = vectorCode.scalarProduct(vectorCode2);
        assertEquals(expected, actual);
    }

    @Test
    public void testSum() throws MyIndexOutOfBoundException {
        int[] b = {0,5,1,0,0,0, 6,4,3,0};
        VectorCode vectorCode2 = new VectorCode(b);
        VectorCode actual = vectorCode.sum(vectorCode2);
        int[] result = {1, 10, 1, 2, 2, 0, 72, 4, 5, 0};
        VectorCode expected = new VectorCode(result);
        assertEquals(expected, actual);
    }


    // тесткейс

// в семестровке один позитвный, один негативный класс

// это unit тесты, тестируем каждый тест отдельно (?)
// (?) интаграционное тестирование. Проверяет мостики между кусками
// регрессия - тест о том, что мы ничего не сломали. Все, от начала до конца. Время работы 1 секунда +-
}
