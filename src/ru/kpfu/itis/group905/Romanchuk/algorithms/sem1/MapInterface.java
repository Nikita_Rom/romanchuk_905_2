package ru.kpfu.itis.group905.Romanchuk.algorithms.sem1;

import java.util.List;
import java.util.Set;

public interface MapInterface<K, V> {
    int START_SIZ = 10;

    V add(K key, V value);
    V remove(K key);
    V get(K key);
    boolean contains(K key);
    void clear();
    int size();
    Set<K> keySet();
    List<V> values();
}
