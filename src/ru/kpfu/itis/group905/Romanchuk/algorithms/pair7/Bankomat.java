package ru.kpfu.itis.group905.Romanchuk.algorithms.pair7;

import java.util.Scanner;

public class Bankomat {
    public static void main(String[] args) {
        int[] money = {5000, 1000, 500, 100, 50};
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0;
        for (int i = 0; i < 5; ++i) {
            int temp = money[i];
            count += n / temp;
            n %= temp;
        }
        System.out.format("%d", count);
    }
}
