package ru.kpfu.itis.group905.Romanchuk.algorithms.pair7;

import java.util.Scanner;

import static java.lang.Integer.max;

public class podArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }
        int best = 0, sum = 0;
        for (int k = 0; k < n; k++) {
            sum = max(array[k], sum+array[k]);
            best = max(best, sum);
        }
        System.out.println(best);
    }
}
