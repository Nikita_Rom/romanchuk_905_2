package ru.kpfu.itis.group905.Romanchuk.algorithms.pair8;

import java.util.Scanner;

public class Task_1 {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        run();
        long finishTime = System.currentTimeMillis();
        System.out.println(finishTime - startTime + "ms");
    }

    private static void run() {
//        Scanner sc = new Scanner(System.in);
//        int x = sc.nextInt();
        int x = 81782;
        binarySearch(x);
    }

    public static void binarySearch(int key) {
        int temp = -1;
        int x = key / 2;
        int low = 0;

        while (!(((x * x) <= key) && ((x + 1) * (x + 1) > key))) {
            if (x * x < key) {
                low = x;
                x = (x + key) / 2;
                temp = x;
            } else if (x * x > key) {
                x = (x + low) / 2;
                temp = x;
            } else if (x * x == key) {
                temp = x;
                break;
            }
        }
        System.out.println(temp);
    }
}
