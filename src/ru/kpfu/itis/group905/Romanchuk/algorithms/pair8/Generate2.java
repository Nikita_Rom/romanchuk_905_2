package ru.kpfu.itis.group905.Romanchuk.algorithms.pair8;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

public class Generate2 {
    public static void main(String[] args) throws FileNotFoundException {
        new Generate2().run();
    }

    private void run() throws FileNotFoundException {
        PrintWriter print = new PrintWriter("input.txt");
        int n = 98251;
        int s = 124597;
        Random random = new Random();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = random.nextInt(1000000);
        }
        int[] b = new int[s];
        for (int i = 0; i < s; i++) {
            b[i] = random.nextInt(1000000);
        }
        Arrays.sort(a);
        Arrays.sort(b);
        print.print(n + s);
        for (int i = n - 1; i >= 0; i--) {
            print.print(" " + a[i]);
        }
        for (int i = 0; i < s; i++) {
            print.print(" " + b[i]);
        }
        int k = random.nextInt(100000);
        print.print(" " + k);
        print.close();
    }
}
