package ru.kpfu.itis.group905.Romanchuk.algorithms.pair8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Task_3 {

    // линейно быстрее считает, чем двоичным...
    
    public static void main(String[] args) throws FileNotFoundException {
        long startTime = System.currentTimeMillis();
        run();
        long finishTime = System.currentTimeMillis();
        System.out.println(finishTime - startTime + "ms");
    }

    private static void run() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int n = sc.nextInt();
        int[] arr = new int[n+1];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        int x = sc.nextInt();

        binarySearch(arr, x, 0, arr.length - 1);
    }


    public static void binarySearch(int[] arr, int key, int low, int high) {
        int temp0 = -1, temp1 = -1;

        while (low <= high) {
            int mid = (low + high) / 2;
            if (arr[mid] < key) {
                low = mid + 1;
            } else if (arr[mid] > key) {
                high = mid - 1;
            } else if (arr[mid] == key) {
                temp0 = temp1 = mid;
                break;
            }
        }
        if ((temp0 != 0) && (temp0 != -1)) {
            if (arr[temp0 - 1] == key) {
                while (arr[temp0 - 1] == key) {
                    int mid = (low + temp0) / 2;
                    if (arr[mid] < key) {
                        low = mid + 1;
                    } else if (arr[mid] > key) {
                        temp0 = mid - 1;
                    } else if (arr[mid] == key) {
                        temp0 = mid;
                        break;
                    }
                }
            }
        }
        if ((temp1 != key) && (temp1 != -1)) {
            if (arr[temp1 + 1] == key) {
                while (arr[temp1 + 1] == key) {
                    int mid = (temp1 + high) / 2;
                    if (arr[mid] < key) {
                        temp1 = mid + 1;
                    } else if (arr[mid] > key) {
                        high = mid - 1;
                    } else if (arr[mid] == key) {
                        temp1 = mid;
                        break;
                    }
                }
            }
        }
        System.out.println("[" + temp0 + ", " + temp1 + "]");
    }

    public static void lineino() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int n = sc.nextInt();
        int k = 0;
        int first = -1, second = -1;
        int[] arr = new int[n+1];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        int x = sc.nextInt();
        for (int i = 0; i < n; i++) {
            if ((arr[i] == x) && (k == 0)) {
                first = i;
                k++;
            }
            if ((arr[i] == x) && (arr[i+1] != x)) {
                second = i;
                break;
            }
        }
        if (second != -1 && first != -1) {
            System.out.println("[" + first + ", " + second + "]");
        } else {
            System.out.println("[-1, -1]");
        }
    }
}
