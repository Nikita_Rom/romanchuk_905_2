package ru.kpfu.itis.group905.Romanchuk.algorithms.pair8;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

public class Generate {
    public static void main(String[] args) throws FileNotFoundException {
        new Generate().run();
    }

    private void run() throws FileNotFoundException {
        PrintWriter print = new PrintWriter("input.txt");
        Random random = new Random();
        int n = 100000;
        int s = random.nextInt(99999);
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = random.nextInt(1000000);
        }
        Arrays.sort(a);
        print.print(n);
        for (int i = s; i < n; i++) {
            print.print(" " + a[i]);
        }
        for (int i = 0; i < s; i++) {
            print.print(" " + a[i]);
        }
        int k = random.nextInt(100000);
        print.print(" " + k);
        print.close();
    }
}
